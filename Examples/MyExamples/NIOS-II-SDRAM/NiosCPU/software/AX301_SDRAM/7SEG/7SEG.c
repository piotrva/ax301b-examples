#include "system.h"
#include "sys/alt_stdio.h"
#include "sys/alt_sys_wrappers.h"
#include "altera_avalon_pio_regs.h"

#include "7SEG.h"

// wzory cyfr
const uint8_t  digits[17]={
	(1<<SEG_A)|(1<<SEG_B)|(1<<SEG_C)|(1<<SEG_D)|(1<<SEG_E)|(1<<SEG_F),//0
	(1<<SEG_B)|(1<<SEG_C),//1
	(1<<SEG_A)|(1<<SEG_B)|(1<<SEG_D)|(1<<SEG_E)|(1<<SEG_G),//2
	(1<<SEG_A)|(1<<SEG_B)|(1<<SEG_C)|(1<<SEG_D)|(1<<SEG_G),//3
	(1<<SEG_B)|(1<<SEG_C)|(1<<SEG_F)|(1<<SEG_G),//4
	(1<<SEG_A)|(1<<SEG_C)|(1<<SEG_D)|(1<<SEG_F)|(1<<SEG_G),//5
	(1<<SEG_A)|(1<<SEG_C)|(1<<SEG_D)|(1<<SEG_E)|(1<<SEG_F)|(1<<SEG_G),//6
	(1<<SEG_A)|(1<<SEG_B)|(1<<SEG_C),//7
	(1<<SEG_A)|(1<<SEG_B)|(1<<SEG_C)|(1<<SEG_D)|(1<<SEG_E)|(1<<SEG_F)|(1<<SEG_G),//8
	(1<<SEG_A)|(1<<SEG_B)|(1<<SEG_C)|(1<<SEG_D)|(1<<SEG_F)|(1<<SEG_G),//9
	(1<<SEG_A)|(1<<SEG_B)|(1<<SEG_C)|(1<<SEG_E)|(1<<SEG_F)|(1<<SEG_G),//A
	(1<<SEG_C)|(1<<SEG_D)|(1<<SEG_E)|(1<<SEG_F)|(1<<SEG_G),//B
	(1<<SEG_A)|(1<<SEG_D)|(1<<SEG_E)|(1<<SEG_F),//C
	(1<<SEG_B)|(1<<SEG_C)|(1<<SEG_D)|(1<<SEG_E)|(1<<SEG_G),//D
	(1<<SEG_A)|(1<<SEG_D)|(1<<SEG_E)|(1<<SEG_F)|(1<<SEG_G),//E
	(1<<SEG_A)|(1<<SEG_E)|(1<<SEG_F)|(1<<SEG_G),//F
	(1<<SEG_G)//-
};

void setDigit(uint8_t digit, uint8_t dp, uint8_t pos){
	if(pos < 6){
		if(digit > 16){  // dla wartosci spoza zakresu gasimy wszystkie segmenty
			IOWR_32DIRECT(SEG7_DISPLAY_BASE, pos * 4, 0|(dp!=0?(1<<SEG_DP):0));
		}else{  // dla pozostalych wartosci wyswietamy odpowiednia liczbe
			IOWR_32DIRECT(SEG7_DISPLAY_BASE, pos * 4, digits[digit] | (dp!=0?(1<<SEG_DP):0));
		}
	}
}

void intDisplayDec(uint32_t number){
	for(uint8_t i = 0 ; i < 6 ; i++){
		setDigit(number % 10, 0, i);
		number /= 10;
	}
}

void intDisplayDecPoint(uint32_t number, uint8_t pointPosition){
	for(uint8_t i = 0 ; i < 6 ; i++){
		setDigit(number % 10, (i == pointPosition), i);
		number /= 10;
	}
}

void intDisplayDecSpace(uint32_t number){
	for(uint8_t i = 0 ; i < 6 ; i++){
		if(i > 0 && number == 0){
			setDigit(OFF, 0, i);
		}else{
			setDigit(number % 10, 0, i);
		}
		number /= 10;
	}
}

void intDisplayHex(uint32_t number){
	for(uint8_t i = 0 ; i < 6 ; i++){
		setDigit(number % 16, 0, i);
		number /= 16;
	}
}
