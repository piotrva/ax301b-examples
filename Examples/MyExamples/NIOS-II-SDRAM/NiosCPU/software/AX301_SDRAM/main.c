#include "system.h"
#include "sys/alt_stdio.h"
#include "io.h"

#include "sys/alt_irq.h"
#include "sys/alt_sys_wrappers.h"
#include "altera_avalon_pio_regs.h"
#include "altera_avalon_timer_regs.h"

#include "7SEG.h"

#define REPORT_PRESC 0x40000

volatile unsigned int i;
volatile unsigned int errors = 0;
volatile unsigned short stop = 0;

void timer0Interrupt(void* context){
	IOWR_ALTERA_AVALON_TIMER_STATUS(TIMER_0_BASE, 0);
	intDisplayHex(i>>4);
	unsigned short btn = IORD_ALTERA_AVALON_PIO_DATA(KEY_BASE);
	if(!(btn & (1<<0))) stop = 1;
	if(!(btn & (1<<1))) stop = 0;
	if(!(btn & (1<<2))) errors = 0;
}

int main()
{ 
	alt_putstr("Hello from Nios II!\r\n");
	IOWR_32DIRECT(SEG7_DISPLAY_BASE, 6*4, 100-1);
	IOWR_32DIRECT(SEG7_DISPLAY_BASE, 7*4, 2000-1);

	IOWR_32DIRECT(LED_PWM_BASE, 16, 0); // preskaller 1 -> podzia� sygna�u zegarowego na 2
	IOWR_32DIRECT(LED_PWM_BASE, 20, 0xFFF); // maksymalna warto�c PWm - rozdzielczo�c

	//warto�ci poszczeg�lnych kana��w
	IOWR_32DIRECT(LED_PWM_BASE, 0, 0);
	IOWR_32DIRECT(LED_PWM_BASE, 4, 0x1F);
	IOWR_32DIRECT(LED_PWM_BASE, 8, 0x200);
	IOWR_32DIRECT(LED_PWM_BASE, 12, 0xFFF);

	alt_ic_isr_register(TIMER_0_IRQ_INTERRUPT_CONTROLLER_ID, TIMER_0_IRQ, timer0Interrupt, NULL, NULL);

	IOWR_ALTERA_AVALON_TIMER_CONTROL(TIMER_0_BASE, ALTERA_AVALON_TIMER_CONTROL_START_MSK |
		ALTERA_AVALON_TIMER_CONTROL_CONT_MSK | ALTERA_AVALON_TIMER_CONTROL_ITO_MSK );

	/* Event loop never exits. */

	while (1){
		for(i = 0 ; i < SDRAM_SPAN ; i += 4){
			IOWR_32DIRECT(SDRAM_BASE, i, i);
			if(i % REPORT_PRESC == 0)alt_printf("      Writing data to SDRAM addr: %x  errors: %x\r\n", i, errors);
			while(stop);
		}
		for(i = 0 ; i < SDRAM_SPAN ; i += 4){
			unsigned int readout = IORD_32DIRECT(SDRAM_BASE, i);
			if(i % REPORT_PRESC == 0)alt_printf("      Reading data from SDRAM addr: %x  errors: %x\r\n", i, errors);
			if(readout != i)alt_printf("[ERR] Error at addr: %x  errors: %x\r\n", i, errors);
			while(stop);
		}

		for(i = 0 ; i < SDRAM_SPAN ; i += 4){
			IOWR_32DIRECT(SDRAM_BASE, i, 0xAAAAAAAA);
			if(i % REPORT_PRESC == 0)alt_printf("      Writing pattern AA to SDRAM addr: %x  errors: %x\r\n", i, errors);
			while(stop);
		}
		for(i = 0 ; i < SDRAM_SPAN ; i += 4){
			unsigned int readout = IORD_32DIRECT(SDRAM_BASE, i);
			if(i % REPORT_PRESC == 0)alt_printf("      Reading pattern AA from SDRAM addr: %x  errors: %x\r\n", i, errors);
			if(readout != 0xAAAAAAAA)alt_printf("[ERR] Error at addr: %x  errors: %x\r\n", i, errors);
			while(stop);
		}

		for(i = 0 ; i < SDRAM_SPAN ; i += 4){
			IOWR_32DIRECT(SDRAM_BASE, i, 0x55555555);
			if(i % REPORT_PRESC == 0)alt_printf("      Writing pattern 55 to SDRAM addr: %x  errors: %x\r\n", i, errors);
			while(stop);
		}
		for(i = 0 ; i < SDRAM_SPAN ; i += 4){
			unsigned int readout = IORD_32DIRECT(SDRAM_BASE, i);
			if(i % REPORT_PRESC == 0)alt_printf("      Reading pattern 55 from SDRAM addr: %x  errors: %x\r\n", i, errors);
			if(readout != 0x55555555)alt_printf("[ERR] Error at addr: %x  errors: %x\r\n", i, errors);
			while(stop);
		}
	}

	return 0;
}
